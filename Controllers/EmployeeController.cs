using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using organizationApi.Models;

namespace organizationApi.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeContext _context;
        private SHA256 sha256Hash = SHA256.Create();

        public EmployeeController(EmployeeContext context)
        {
            _context = context;
        }

        // GET: api/Employee
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Employee>>> GetEmployees()
        {
            return await _context.Employees.ToListAsync();
        }

        // GET: api/Employee/Subordinate/1
        [HttpGet("Subordinate/{id}")]
        public async Task<ActionResult<IEnumerable<Employee>>> GetSubordinates(string id)
        {
            var _id = new SqlParameter("@id", id);
            var employees = await _context.Employees.FromSqlRaw("exec getManagerSubordinates @id", _id).ToListAsync();
            return employees; 
        }

        // GET: api/Employee/MyManager/1
        [HttpGet("MyManager/{id}")]
        public async Task<ActionResult<Employee>> GetMyManager(string id)
        {
            var _id = new SqlParameter("@id", id);
            var managers = await _context.Employees.FromSqlRaw("exec getMyManager @id", _id).ToListAsync();
            if (managers.Count > 1 || managers.Count == 0 ) {
                return NoContent();
            } else if (managers.Count == 1) {
                return managers.First();
            } else {
                return BadRequest();
            }
        }

// should be in another controller for tasks (leaving here due to time limitations)
        // GET: api/Employee/Tasks/1
        [HttpGet("Task/{id}")]
        public async Task<ActionResult<IEnumerable<Models.Task>>> GetTasks(string id)
        {
            var _id = new SqlParameter("@id", id);
            var tasks = await _context.Tasks.FromSqlRaw("exec getEmployeeTasks @id", _id).ToListAsync();
            if (tasks.Count > 0) {
                return tasks; 
            } else {
                return NoContent();
            }
        }

        // GET: api/Employee/Tasks/1
        [HttpGet("Reports/{id}")]
        public async Task<ActionResult<IEnumerable<Report>>> GetReports(string id)
        {
            var _id = new SqlParameter("@id", id);
            var report = await _context.Reports.FromSqlRaw("exec getManagerReports @id", _id).ToListAsync();
             if (report.Count > 0) {
                return report; 
            } else {
                return NoContent();
            }
        }

        // GET: api/Employee/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Employee>> GetEmployee(string id)
        {
            var employee = await _context.Employees.FindAsync(id);

            if (employee == null)
            {
                return NotFound();
            }

            return employee;
        }

        // PUT: api/Employee/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmployee(string id, Employee employee)
        {
            if (id != employee.id)
            {
                return BadRequest();
            }

            _context.Entry(employee).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Employee
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Employee>> PostEmployee(Employee employee)
        {
            _context.Employees.Add(employee);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (EmployeeExists(employee.id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetEmployee", new { id = employee.id }, employee);
        }

        // POST: api/Employee/Report
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [EnableCors("AllowCors")]
        [HttpPost]
        [Route("Report")]
        public async Task<ActionResult<Report>> PostReport(Report report)
        {
            string date = DateTime.Now.ToString();
            string hash = Models.Report.GetHash(sha256Hash, date);
            report.id = hash;
           
            try
            {
                 _context.Reports.Add(report);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                if (ReportExists(report.id))
                {
                    return Conflict();
                }
                else
                {
                    throw ex;
                }
            }

            return CreatedAtAction("GetReports", new { id = report.managerId }, report);
        }

        // POST: api/Employee/AssignTask
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [EnableCors("AllowCors")]
        [HttpPost]
        [Route("AssignTask")]
        public async Task<ActionResult<Models.Task>> PostAssignTask(Models.Task task)
        {
            string date = DateTime.Now.ToString();
            string hash = Models.Task.GetHash(sha256Hash, date);
            task.id = hash;
            Console.WriteLine("hash value >>> {0}", hash);
           
            try
            {
                 _context.Tasks.Add(task);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                if (TaskExists(task.id))
                {
                    return Conflict();
                }
                else
                {
                    throw ex;
                }
            }

            return CreatedAtAction("GetTask", new { id = task.empId }, task);
        }


        // DELETE: api/Employee/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee(string id)
        {
            var employee = await _context.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }

            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool EmployeeExists(string id)
        {
            return _context.Employees.Any(e => e.id == id);
        }
        private bool TaskExists(string id)
        {
            return _context.Tasks.Any(t => t.id == id);
        }
        private bool ReportExists(string id)
        {
            return _context.Reports.Any(t => t.id == id);
        }
    }
}
