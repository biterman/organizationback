using System;

namespace organizationApi.Models {
    public interface ISubordinates
    {
        string employeeId {get; set;}
        string managerId {get; set;}

    }
}