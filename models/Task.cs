using System;

namespace organizationApi.Models {
    public class Task: BaseModel {
        public string empId {get; set;}
        public string text { get; set; }
        public DateTime dueDate{ get; set; }
        public DateTime assignDate{ get; set; }

    }
}