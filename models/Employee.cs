namespace organizationApi.Models {
    public class Employee {
        public string id {get; set;}
        public string firstName {get; set;}
        public string lastName {get; set;}
        public string position {get; set;}
        public bool isManager {get; set;}
        public string report() {
            return "";
        }
    }
}