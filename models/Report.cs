using System;

namespace organizationApi.Models
{
    public class Report: BaseModel {

        public string text { get; set; }
        public DateTime date { get; set; }
        public string empId { get; set; }
        public string managerId { get; set; }
        

    }
}