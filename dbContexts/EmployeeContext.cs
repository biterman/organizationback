using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Sqlite;

namespace organizationApi.Models {
    public class EmployeeContext : DbContext {
        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // {
        //     var connectionStringBuilder = new SqliteConnectionStringBuilder { DataSource = "organization.db" };
        //     var connectionString = connectionStringBuilder.ToString();
        //     var connection = new SqliteConnection(connectionString);
        //     optionsBuilder.UseSqlite(connection);
        // }

        public EmployeeContext(DbContextOptions<EmployeeContext> options)
            : base(options)
        {
        }


        public DbSet<Employee> Employees {get; set;}
        public DbSet<Task> Tasks {get; set;}
        public DbSet<Report> Reports {get; set;}
    }
}